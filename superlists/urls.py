from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
                       url(r'^$', 'AppSurvey.views.home_page', name='home'),
                       #url(r'add/$',
                           #'AppSurvey.views.add_page', name='add'),
                       url(r'ShowResult/$',
                           'AppSurvey.views.ShowResult',
                           name='ShowResult'),
                       url(r'Delete/$',
                           'AppSurvey.views.DeletePage',
                           name='DeletePage'),
                       )

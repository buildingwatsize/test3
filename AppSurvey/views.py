from django.shortcuts import redirect, render
from AppSurvey.models import Survey
from django.forms import ModelForm


from django.utils import timezone
from datetime import datetime, timedelta


class QuestionAndAnswer:
    def __init__(self, question, ans, n1, n2, time):
        self.question = question
        self.ans = ans
        self.n1 = n1
        self.n2 = n2
        self.time = time


timenow = timezone.now()  



def home_page(request):
    days = 99999
    minutes = 0
    if request.method == 'POST' and request.POST.get(
                                    'tenmins', '') == 'last 10 Minutes':
        days = 0
        minutes = 10
    if request.method == 'POST' and request.POST.get(
                                    'today', '') == 'Today':
        days = 1
    if request.method == 'POST' and request.POST.get(
                                    'week', '') == 'Week':
        days = 7
    if request.method == 'POST' and request.POST.get(
                                    'month', '') == 'Month':
        days = 30 
    if request.method == 'POST' and request.POST.get(
                                    'year', '') == 'Year':
        days = 365

    timetofilter = timenow - timedelta(days=days, minutes=minutes)

    if request.method == 'POST' and request.POST.get(
                                    'add_qa', '') == 'Submit':        
        q = request.POST['q_text']
        a1 = request.POST['a_text']
        a2 = request.POST['a2_text']
        Survey.objects.create(question=q,
                              answer1=a1,
                              answer2=a2,
                              pub_date=timenow)
        return redirect('/')

    qa = Survey.objects.filter(pub_date__range=(timetofilter, timenow))
    
    return render(request, 'home.html', {'questions': qa, 't': timenow})


def ShowResult(request):
    qa = Survey.objects.all()
    qanda = []
    if request.method == 'POST' and request.POST.get(
                                    'submit', '') == 'Submit': 
        for i in range(len(qa)):
            Q = qa[i].question
            A = request.POST[Q]
            if A == qa[i].answer1:
                qa[i].N_1 = int(qa[i].N_1)+1
            if A == qa[i].answer2:
                qa[i].N_2 = int(qa[i].N_2)+1
            qa[i].save()
            qanda.append(QuestionAndAnswer(Q, A, qa[i].N_1, qa[i].N_2, qa[i].pub_date))
    return render(request, 'show.html', {'QandA': qanda})


def DeletePage(request):

    qa = Survey.objects.all()

    if request.method == 'POST' and request.POST.get(
                                   'deleted', '') == 'Deleted':
        deleted = request.POST.getlist('list_delete')
        Survey.objects.filter(id__in=deleted).delete()
        return redirect('/')
    return render(request, 'delete.html', {'questions': qa})
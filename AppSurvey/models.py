import datetime

from django.db import models
from django.utils import timezone
from datetime import datetime


class Survey(models.Model):
    question = models.TextField("")
    answer1 = models.TextField("")
    answer2 = models.TextField("")
    N_1 = models.IntegerField(default=0)
    N_2 = models.IntegerField(default=0)
    pub_date = models.DateTimeField(timezone.now())
